package pl.edu.pwr.s249285.hello;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

import java.util.ArrayList;

public class Bird extends MainObject {

    private ArrayList<Bitmap> listOfBirdsBitmap = new ArrayList<>();
    private int counter, flap, currentBitmap;
    private float fall;
    public Bird() {
        this.counter = 0;
        this.flap = 5;
        this.currentBitmap = 0;
        this.fall = 0;
    }

    public void draw(Canvas canvas){
        fall();
        canvas.drawBitmap(this.getBitmap(),this.x,this.y,null);

    }

    private void fall() {
        this.fall+=0.6;
        this.y+=this.fall;
    }

    public ArrayList<Bitmap> getListOfBirdsBitmap() {
        return listOfBirdsBitmap;
    }

    public void setListOfBirdsBitmap(ArrayList<Bitmap> listOfBirdsBitmap) {
        this.listOfBirdsBitmap = listOfBirdsBitmap;

        for (int i = 0; i < listOfBirdsBitmap.size(); i++){
            this.listOfBirdsBitmap.set(i,Bitmap.createScaledBitmap(this.listOfBirdsBitmap.get(i),this.width,this.height,true));
        }
    }

    @Override
    public Bitmap getBitmap() {
        counter++;
                if(this.counter == this.flap){ //change bitmap
                    for (int i = 0; i <listOfBirdsBitmap.size() ; i++) {
                        if (i == listOfBirdsBitmap.size() - 1){
                            this.currentBitmap = 0;
                            break;
                        }
                        else if (this.currentBitmap == i){
                            currentBitmap = i+1;
                            break;
                        }


                    }
                    counter = 0;
                }
                if (this.fall < 0){
                    Matrix matrix = new Matrix();
                    matrix.postRotate(-25);
                    return Bitmap.createBitmap(listOfBirdsBitmap.get(currentBitmap),0,0,listOfBirdsBitmap.get(currentBitmap).getWidth(),listOfBirdsBitmap.get(currentBitmap).getHeight(),matrix,true);
                }
               else if (fall>=0){
                   Matrix matrix = new Matrix();
                   if(fall > 70){
                       matrix.postRotate(-25+(fall*2));
                   }
                    else{
                        matrix.postRotate(45);
                   }
                    return Bitmap.createBitmap(listOfBirdsBitmap.get(currentBitmap),0,0,listOfBirdsBitmap.get(currentBitmap).getWidth(),listOfBirdsBitmap.get(currentBitmap).getHeight(),matrix,true);
                }
                return this.listOfBirdsBitmap.get(currentBitmap);
    }

    public float getFall() {
        return fall;
    }

    public void setFall(float fall) {
        this.fall = fall;
    }
}

