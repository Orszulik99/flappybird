package pl.edu.pwr.s249285.hello;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class GameViev extends View {

    private Bird bird;
    private android.os.Handler handler;
    private Runnable runnable;
    private ArrayList<Pipe> arrayOfPipies;
    private int  sumOfPipe, distanceBetweenPipe;
    private int score;
    private boolean isStart = false, restart = false;

    public GameViev(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        score = 0;
        initTheBird();
        initThePipe();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
            invalidate();
            }
        };
    }

    private void initThePipe() {
        sumOfPipe = 6;
        distanceBetweenPipe = 500*DataGame.SCREEN_HEIGHT/1920;
        arrayOfPipies = new ArrayList<>();
        for (int i = 0; i < sumOfPipe; i++) {
            if (i<sumOfPipe/2){
                this.arrayOfPipies.add(new Pipe(DataGame.SCREEN_WIDTH+i*((DataGame.SCREEN_WIDTH+200*DataGame.SCREEN_WIDTH/1080)/(sumOfPipe/2)),0,200*DataGame.SCREEN_WIDTH/1080,DataGame.SCREEN_HEIGHT/2));
                this.arrayOfPipies.get(this.arrayOfPipies.size()-1).setBitmap(BitmapFactory.decodeResource(this.getResources(),R.drawable.pipe2));
                this.arrayOfPipies.get(this.arrayOfPipies.size() - 1).randomHeightOfPipe();
            }else{
                this.arrayOfPipies.add(new Pipe(this.arrayOfPipies.get(i-sumOfPipe/2).getX(),this.arrayOfPipies.get(i-sumOfPipe/2).getY() + this.arrayOfPipies.get(i-sumOfPipe/2).getHeight() + this.distanceBetweenPipe,200*DataGame.SCREEN_WIDTH/1080,DataGame.SCREEN_HEIGHT/2));
                this.arrayOfPipies.get(this.arrayOfPipies.size()-1).setBitmap(BitmapFactory.decodeResource(this.getResources(),R.drawable.pipe1));
            }
        }
    }

    private void initTheBird() {
        bird = new Bird();
        bird.setWidth(100*DataGame.SCREEN_WIDTH/1080);
        bird.setHeight(100*DataGame.SCREEN_HEIGHT/1920);
        bird.setX(100*DataGame.SCREEN_WIDTH/1080);
        bird.setY(DataGame.SCREEN_HEIGHT/2-bird.getHeight()/2);
        ArrayList<Bitmap> arrayBirdBitMap = new ArrayList<>();
        arrayBirdBitMap.add(BitmapFactory.decodeResource(this.getResources(),R.drawable.bird1));
        arrayBirdBitMap.add(BitmapFactory.decodeResource(this.getResources(),R.drawable.bird2));
        bird.setListOfBirdsBitmap(arrayBirdBitMap);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        // canvas.drawColor(Color.BLUE);
        if (isStart) {
            bird.draw(canvas);
            for (int i = 0; i < sumOfPipe; i++) {
                if (bird.getRectangleAroundTheObject().intersect(arrayOfPipies.get(i).getRectangleAroundTheObject()) || bird.getY() - bird.getHeight() < 0 || bird.getY() > DataGame.SCREEN_HEIGHT) {           //gameover
                    Pipe.speedOfPipe = 0;
                    MainActivity.txt_score_over.setText(MainActivity.txt_score.getText());
                    MainActivity.txt_score.setVisibility(INVISIBLE);
                    MainActivity.rl_game_over.setVisibility(VISIBLE);

                }
                if (this.bird.getX() + this.bird.getWidth() > arrayOfPipies.get(i).getX() + arrayOfPipies.get(i).getWidth() / 2 &&                                               // score
                        this.bird.getX() + this.bird.getWidth() <= arrayOfPipies.get(i).getX() + arrayOfPipies.get(i).getWidth() / 2 + Pipe.speedOfPipe && i < sumOfPipe / 2) {
                    score++;
                    MainActivity.txt_score.setText("" + score);

                }
                if (this.arrayOfPipies.get(i).getX() < -arrayOfPipies.get(i).getWidth()) {
                    this.arrayOfPipies.get(i).setX(DataGame.SCREEN_WIDTH);
                    if (i < sumOfPipe / 2) {
                        arrayOfPipies.get(i).randomHeightOfPipe();
                    } else {
                        arrayOfPipies.get(i).setY(this.arrayOfPipies.get(i - sumOfPipe / 2).getY() + this.arrayOfPipies.get(i - sumOfPipe / 2).getHeight() + this.distanceBetweenPipe);
                    }
                }
                this.arrayOfPipies.get(i).draw(canvas);
            }
        }else{
            if(bird.getY() > DataGame.SCREEN_HEIGHT/2){
                bird.setFall(-15*DataGame.SCREEN_HEIGHT/1920);          //
            }
            bird.draw(canvas);
        }
            handler.postDelayed(runnable, 10);
        }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN){
        bird.setFall(-15);
        }
        return true;
    }


    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    public void resetGame() {
        MainActivity.txt_score.setText("0");
        score = 0;
        initThePipe();
        initTheBird();

    }
}
