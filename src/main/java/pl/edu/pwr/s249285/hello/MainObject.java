
package pl.edu.pwr.s249285.hello;

import android.graphics.Bitmap;
import android.graphics.Rect;

public class MainObject {

    protected float x;
    protected float y;
    protected int width;
    protected int height;
    protected Bitmap bitmap;
    protected Rect rectangleAroundTheObject;


    public MainObject(float x, float y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public MainObject(float x, float y, int width, int height, Bitmap bitmap) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.bitmap = bitmap;
    }

    public MainObject() {

    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Rect getRectangleAroundTheObject() {
        return new Rect((int)this.x,(int)this.y,(int)x+this.width,(int)this.y+this.height);
    }

    public void setRectangleAroundTheObject(Rect rectangleAroundTheObject) {
        this.rectangleAroundTheObject = rectangleAroundTheObject;
    }
}
