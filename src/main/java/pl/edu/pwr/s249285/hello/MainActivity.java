package pl.edu.pwr.s249285.hello;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    public static TextView txt_score,txt_score_over,play_1_more;
    public static RelativeLayout rl_game_over;
    public static Button button_start;
    private GameViev gv;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide status  bar

        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        DataGame.SCREEN_WIDTH = displayMetrics.widthPixels;
        DataGame.SCREEN_HEIGHT = displayMetrics.heightPixels;
        setContentView(R.layout.activity_main);
        txt_score = findViewById(R.id.txt_score);
        txt_score_over = findViewById(R.id.gameOver);
        rl_game_over = findViewById(R.id.gameOverLayout);
       button_start = findViewById(R.id.button_start);
       gv =  findViewById(R.id.gv);
       button_start.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               gv.setStart(true);
               txt_score.setVisibility(View.VISIBLE);
               button_start.setVisibility(View.INVISIBLE);
           }
       });
       rl_game_over.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               button_start.setVisibility(View.VISIBLE);
               rl_game_over.setVisibility(View.INVISIBLE);
             //  play_1_more.setVisibility(View.INVISIBLE);
               gv.setStart(false);
               gv.resetGame();
           }
       });
    }


}