package pl.edu.pwr.s249285.hello;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.Random;

public class Pipe extends MainObject{

    public static int speedOfPipe;

    public Pipe(float x, float y, int width, int height) {
        super(x,y,width,height);
        speedOfPipe = 30*DataGame.SCREEN_WIDTH/1080;
    }

    public void draw(Canvas canvas){
        this.x-=speedOfPipe;       //poistion of pipe x-direction  right->left
        canvas.drawBitmap(this.bitmap,this.x,this.y,null);
    }

    public void randomHeightOfPipe(){
        Random random = new Random();
        this.y = random.nextInt((this.height/4)) - this.height/4;

    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = Bitmap.createScaledBitmap(bitmap,width,height,true);
    }
}
